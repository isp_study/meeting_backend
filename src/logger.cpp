#include <Poco/AutoPtr.h>
#include <Poco/FormattingChannel.h>
#include <Poco/Logger.h>
#include <Poco/PatternFormatter.h>
#include <Poco/SimpleFileChannel.h>
#include <logger.hpp>

namespace logger {

using Poco::AutoPtr;
using Poco::FormattingChannel;
using Poco::PatternFormatter;

Poco::Logger &InitLogger() {
	AutoPtr<Poco::SimpleFileChannel> file_channel(new Poco::SimpleFileChannel);
	file_channel->setProperty("path", "./meeting_backend.log");
	AutoPtr<PatternFormatter> formatter(new PatternFormatter);
	formatter->setProperty("pattern", "%Y-%m-%d %H:%M:%S %s: %t");
	AutoPtr<FormattingChannel> channel(new FormattingChannel(formatter, file_channel));
	Poco::Logger::root().setChannel(channel);
	Poco::Logger::root().setLevel("trace");
	return Poco::Logger::get("Logger");
}
Poco::Logger &GetLogger() {
	static Poco::Logger &logger{InitLogger()};
	return logger;
}
} // namespace logger
