#include <fstream>
#include <Poco/Data/Session.h>
#include <Poco/Data/SQLite/Connector.h>
#include <Poco/Data/SQLite/Utility.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/ServerSocketImpl.h>
#include <handlers/factory.hpp>
#include <iostream>
#include <server.hpp>
#include <sqlite.hpp>
#include <logger.hpp>

namespace {

using Poco::Net::Socket;
using Poco::Data::Keywords::now;

class ServerSocketImpl : public Poco::Net::ServerSocketImpl {
public:
	using Poco::Net::SocketImpl::init;
};

class ServerSocket : public Socket {
public:
	ServerSocket(const std::string &address, const int port) : Poco::Net::Socket(new ServerSocketImpl()) {
		const auto socket_address = MakeAddress(address, port);
		ServerSocketImpl *socket = static_cast<ServerSocketImpl *>(impl());
		socket->init(socket_address.af());
		socket->setReuseAddress(true);
		socket->setReusePort(false);
		socket->bind(socket_address, false);
		socket->listen();
	}
private:
	Poco::Net::SocketAddress MakeAddress(const std::string &address, const int port) {
		if (address.empty() || address == "*") {
			return Poco::Net::SocketAddress(port);
		}
		return Poco::Net::SocketAddress(address + ":" + std::to_string(port));
	}
};

} // anonymous namespace

int Server::main(const std::vector<std::string> & args) {
    logger::GetLogger().information("Start Server");
    Poco::Data::SQLite::Connector::registerConnector();
    Poco::Data::SQLite::Utility::setThreadMode(Poco::Data::SQLite::Utility::THREAD_MODE_SINGLE);
    if (std::find(args.begin(), args.end(), "init-db") != args.end()) {
        Poco::Data::Session session(sqlite::TYPE_SESSION, sqlite::DB_PATH);
        std::ifstream schema_stream(sqlite::SCHEMA_PATH);
        session << schema_stream.rdbuf(), now; // выполнить sql схему
    }
	auto *parameters = new Poco::Net::HTTPServerParams();
	parameters->setTimeout(10000);
	parameters->setMaxQueued(100);
	parameters->setMaxThreads(1);

	const Poco::Net::ServerSocket socket(ServerSocket("*", 1500));

	Poco::Net::HTTPServer server(new handlers::Factory(), socket, parameters);

	server.start();
	waitForTerminationRequest();
	server.stopAll();

	return 0;
}
