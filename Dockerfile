FROM ubuntu:18.10

COPY build/lib/* /lib/x86_64-linux-gnu/
COPY build/bin /
COPY build/etc/schema.sql /etc/

CMD ["/ISPMeetings","init-db"]
