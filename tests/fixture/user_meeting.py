import pytest
import socket
import os
import hamcrest as hc
from termcolor import colored
import api
from meeting.rest import ApiException
from api.meeting.api_client import ApiClient
from api.meeting.api.user_meetings_api import UserMeetingsApi
from api.meeting.models.new_meeting import NewMeeting


def gprint(text):
    print(colored(text, 'green'))


def meeting_server_address():
    return socket.gethostbyname(os.environ["SSH_HOST"])


@pytest.fixture(scope='session')
def client():
    client = ApiClient()
    server_address = meeting_server_address()
    client.configuration.host = f"http://{server_address}:8080"
    client.host = client.configuration.host
    return client


@pytest.fixture
def custom_user_meeting(request, client):
    """
    Фикстура для создания встречи
    * Создает встречу
    * Проверяет, что полученные данные соотвествуют переданным
    * Получает инфу о встрече
    * Проверяет эту информацию
    * Удаляет встречу
    """
    def _custom_user_meeting(schema: NewMeeting):
        gprint("Create new meeting with schema: ")
        print(f"{schema}")
        #Создаём встечу
        create_result = UserMeetingsApi(client)\
                        .create_meeting(schema)
        gprint("Create new meeting result:")
        print(f"{create_result}")

        def final(client, meeting_id):
            """
            Удаляем встречу
            """
            gprint(f"Delete meeting No {meeting_id}")
            delete_result = UserMeetingsApi(client)\
                            .delete_meeting(meeting_id)
            gprint(f"Delete meeting No {meeting_id} result:")
            print(f"{delete_result}")

            hc.assert_that(hc.calling(
                           lambda: UserMeetingsApi(client)\
                           .get_meeting(meeting_id)),
                           hc.raises(ApiException, ".*(403).*")) #Код 403

        request.addfinalizer(lambda: final(client, create_result.id))

        hc.assert_that(create_result,
                       hc.has_properties(id=hc.not_none(),
                                         name=schema.name,
                                         description=schema.description,
                                         address=schema.address,
                                         signup_description=schema.signup_description,
                                         signup_from_date=schema.signup_from_date,
                                         signup_to_date=schema.signup_to_date,
                                         from_date=schema.from_date,
                                         to_date=schema.to_date,
                                         published=schema.published))

        #Получим встречу по ID
        meeting_info = UserMeetingsApi(client)\
                       .get_meeting(create_result.id)
        gprint("Get created meeting info:")
        print(f"{meeting_info}")
        hc.assert_that(meeting_info == create_result)

        return meeting_info
    return _custom_user_meeting
