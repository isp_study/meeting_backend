import pytest
import string
import random
import time
import hamcrest as hc
from termcolor import colored
import api
from meeting.rest import ApiException
from api.meeting.api.user_meetings_api import UserMeetingsApi
from api.meeting.models.new_meeting import NewMeeting
from api.meeting.models.meeting import Meeting


#TODO (anyone) вынести в отдельный файл
def gprint(text):
    print(colored(text, 'green'))

def time_generator():
    return random.randint(0, int(time.time()))


def name_generator(size=6, chars=string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def gen_new_meeting():
    return NewMeeting(name=name_generator(),
                      description=name_generator(20),
                      address=name_generator(10),
                      signup_description=name_generator(20),
                      signup_from_date=time_generator(),
                      signup_to_date=time_generator(),
                      from_date=time_generator(),
                      to_date=time_generator(),
                      published=random.choice([True, False]))


def test_meeting_list(client, custom_user_meeting):
    """
    Проверим список встреч
    """
    meeting_list = UserMeetingsApi(client).user_meetings()
    gprint("Meeting list:")
    print(f"{meeting_list}")
    hc.assert_that(meeting_list, hc.empty())

    schema = gen_new_meeting()
    user_meeting = custom_user_meeting(schema)

    meeting_list = UserMeetingsApi(client).user_meetings()
    gprint("Meeting list with one element:")
    print(f"{meeting_list}")
    hc.assert_that(meeting_list, hc.only_contains(user_meeting))

    schema = gen_new_meeting()
    another_user_meeting = custom_user_meeting(schema)

    meeting_list = UserMeetingsApi(client).user_meetings()
    gprint("Meeting list with two elements:")
    print(f"{meeting_list}")
    hc.assert_that(meeting_list, hc.has_item(user_meeting))
    hc.assert_that(meeting_list, hc.has_item(another_user_meeting))
    hc.assert_that(another_user_meeting.id, hc.is_not(hc.equal_to(user_meeting.id)))


def test_meeting_edit(client, custom_user_meeting):
    """
    Проверим редактирование встречи
    """
    schema = gen_new_meeting()
    user_meeting = custom_user_meeting(schema)

    modify_schema = Meeting(id=user_meeting.id,
                            name=name_generator(),
                            description=name_generator(20),
                            address=name_generator(10),
                            signup_description=name_generator(20),
                            signup_from_date=time_generator(),
                            signup_to_date=time_generator(),
                            from_date=time_generator(),
                            to_date=time_generator(),
                            published=random.choice([True, False]))
    gprint("Modify schema:")
    print(f"{modify_schema}")

    modify_result = UserMeetingsApi(client).modify_meeting(user_meeting.id, modify_schema)
    gprint("Modify result:")
    print(f"{modify_result}")
    hc.assert_that(modify_result.to_dict(), hc.equal_to(modify_schema.to_dict()))

    get_after_modify = UserMeetingsApi(client).get_meeting(user_meeting.id)
    gprint("Get meeting info after modify:")
    print(f"{get_after_modify}")
    hc.assert_that(get_after_modify.to_dict(), hc.equal_to(modify_schema.to_dict()))


def test_negative_get_meeting(client):
    """
    Получаем несуществующую встречу
    """
    hc.assert_that(hc.calling(
                   lambda: UserMeetingsApi(client)\
                   .get_meeting(666)),
                   hc.raises(ApiException, ".*(403).*"))


def test_negative_edit_meeting(client):
    """
    Редактируем несуществующую встречу
    """
    hc.assert_that(hc.calling(
                   lambda: UserMeetingsApi(client)\
                   .modify_meeting(666, Meeting())),
                   hc.raises(ApiException, ".*(403).*"))


def test_negative_delete_meeting(client):
    """
    Удалчем несуществующую встречу
    """
    hc.assert_that(hc.calling(
                   lambda: UserMeetingsApi(client)\
                   .delete_meeting(666)),
                   hc.raises(ApiException, ".*(403).*"))
