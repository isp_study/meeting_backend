#include <Poco/Data/Session.h>
#include <Poco/Data/SQLite/Connector.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <handlers.hpp>
#include <logger.hpp>
#include <nlohmann/json.hpp>
#include <user_meeting.hpp>
#include <sqlite.hpp>

namespace handlers {

using MeetingList = std::vector<Meeting>;

/*
class MapStorage: public Storage {
public:
	void Save(Meeting& meeting) override {
		if (meeting.id.has_value()) {
			m_storage[meeting.id.value()] = meeting;
		} else {
			int id = m_storage.size();
			meeting.id = id;
			m_storage[id] = meeting;
		}
	}

	MeetingList GetList() override {
		MeetingList result;
		for (auto [_, meeting]: m_storage) {
			result.push_back(meeting);
		}
		return result;
	}

	std::optional<Meeting> Get(int id) override {
		if (m_storage.find(id) != m_storage.end()) {
			return m_storage[id];
		}
		return std::nullopt;
	}

	void Delete(int id) override {
		m_storage.erase(id);
	}

private:
	using MeetingMap = std::map<int, Meeting>;
	MeetingMap m_storage;
};*/

using nlohmann::json;
using Poco::Data::Keywords::into;
using Poco::Data::Keywords::now;
using Poco::Data::Keywords::range;
using Poco::Data::Keywords::use;
using Poco::Data::Statement;

class SqliteStorage : public Storage {
public:
	void Save(Meeting &meeting) override {
		Guard g{m_sqlite};
		if (meeting.id.has_value()) {
			Statement update(m_session);
			auto published = b2i(meeting.published);
			update << "UPDATE meeting SET "
					  "name=?, description=?, address=?, "
					  "signup_description=?, signup_from_date=?, "
					  "signup_to_date=?, from_date=?, to_date=?, published=?"
					  "WHERE id=?",
					use(meeting.name),
					use(meeting.description),
					use(meeting.address),
					use(meeting.signup_description),
					use(meeting.signup_from_date),
					use(meeting.signup_to_date),
					use(meeting.from_date),
					use(meeting.to_date),
					use(published),
					use(meeting.id.value()),
					now;
		} else {
			Statement insert(m_session);
			int published = b2i(meeting.published);
			insert << "INSERT INTO meeting "
					  "(name, description, address, signup_description, "
					  "signup_from_date, signup_to_date, from_date, to_date, published) "
					  "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)",
					use(meeting.name),
					use(meeting.description),
					use(meeting.address),
					use(meeting.signup_description),
					use(meeting.signup_from_date),
					use(meeting.signup_to_date),
					use(meeting.from_date),
					use(meeting.to_date),
					use(published),
					now;

			Statement select(m_session);
			int id = 0;
			select << "SELECT last_insert_rowid()", into(id), now;
			meeting.id = id;
		}
	}

	Storage::MeetingList GetList() override {
		Guard g{m_sqlite};
		Storage::MeetingList list;
		Meeting meeting;
		Statement select(m_session);
		select << "SELECT "
				  "id, name, description, address, signup_description, "
				  "signup_from_date, signup_to_date, from_date, to_date, published "
				  "FROM meeting",
				into(meeting.id.emplace()),
				into(meeting.name),
				into(meeting.description),
				into(meeting.address),
				into(meeting.signup_description),
				into(meeting.signup_from_date),
				into(meeting.signup_to_date),
				into(meeting.from_date),
				into(meeting.to_date),
				into(meeting.published),
				range(0, 1); //  iterate over result set one row at a time

		while (!select.done() && select.execute() > 0) {
			list.push_back(meeting);
		}
		return list;
	}

	std::optional<Meeting> Get(int id) override {
		Guard g{m_sqlite};
		int cnt = 0;
		m_session << "SELECT COUNT(*) FROM meeting WHERE id=?", use(id), into(cnt), now;
		if (cnt > 0) {
			Meeting meeting;
			Statement select(m_session);
			int tmp_id = 0;
			select << "SELECT "
					  "id, name, description, address, signup_description, "
					  "signup_from_date, signup_to_date, from_date, to_date, published "
					  "FROM meeting WHERE id=?",
					use(id),
					into(tmp_id),
					into(meeting.name),
					into(meeting.description),
					into(meeting.address),
					into(meeting.signup_description),
					into(meeting.signup_from_date),
					into(meeting.signup_to_date),
					into(meeting.from_date),
					into(meeting.to_date),
					into(meeting.published),
					now;
			meeting.id = tmp_id;
			return meeting;
		}
		return std::nullopt;
	}

	void Delete(int id) override {
	    Guard g{m_sqlite};
	    m_session << "DELETE FROM meeting WHERE id=?", use(id), now;
        }

private:
	std::mutex m_sqlite;
	using Guard = std::lock_guard<std::mutex>;
	Poco::Data::Session m_session{sqlite::TYPE_SESSION, sqlite::DB_PATH};

	int b2i(bool b) {
		return b ? 1 : 0;
	}
};

Storage& GetStorage() {
	static SqliteStorage storage;
	return storage;
}

void UserMeetingList::HandleRestRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
	nlohmann::json result = nlohmann::json::array({});
	auto& storage = GetStorage();
	for (auto meeting: storage.GetList()) {
		result.push_back(meeting);
	}
	logger::GetLogger().information("UserMeetingList: " + result.dump());
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	response.send() << result;

}

void UserMeetingCreate::HandleRestRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
	nlohmann::json body = nlohmann::json::parse(request.stream());
	logger::GetLogger().information("UserMeetingCreate: " + body.dump());
	auto& storage = GetStorage();
	auto meeting = body.get<Meeting>();
	storage.Save(meeting);
	to_json(body, meeting);
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	response.send() << body;
}

void UserMeetingRead::HandleRestRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
	auto& storage = GetStorage();
	auto meeting = storage.Get(m_id);
	if (meeting.has_value()) {
		nlohmann::json result = meeting.value();
		logger::GetLogger().information("UserMeetingRead: " + result.dump());
		response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
		response.send() << result;
	}
	logger::GetLogger().information("UserMeetingRead: meeting " + std::to_string(m_id) + " not found");
	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_NOT_FOUND);
	response.send();
}

void UserMeetingUpdate::HandleRestRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
	logger::GetLogger().information("UserMeetingUpdate");
	nlohmann::json body = nlohmann::json::parse(request.stream());
	auto& storage = GetStorage();
	auto meeting = body.get<Meeting>();
	meeting.id = m_id;
	storage.Save(meeting);

	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
	response.send() << nlohmann::json(meeting);
}

void UserMeetingDelete::HandleRestRequest(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response) {
	logger::GetLogger().information("UserMeetingDelete");
	auto& storage = GetStorage();
	storage.Delete(m_id);

	response.setStatus(Poco::Net::HTTPServerResponse::HTTP_NO_CONTENT);
	response.send();
}


} // namespace handlers
