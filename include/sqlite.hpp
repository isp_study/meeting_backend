#pragma once

namespace sqlite {
constexpr auto TYPE_SESSION = "SQLite";
constexpr auto DB_PATH = "sample.db";
constexpr auto SCHEMA_PATH = "etc/schema.sql";
} // namespace sqlite