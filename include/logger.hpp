#pragma once
#include "Poco/Logger.h"

namespace logger {

Poco::Logger &GetLogger();

}// namespace logger
